// { begin copyright } 
// Copyright Sathish Gopalakrishnan 2019
// 
// This file is part of Pennyworth.
// 
// Pennyworth is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Pennyworth is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Pennyworth.  If not, see <http://www.gnu.org/licenses/>.
// 
// { end copyright } 
 
 

package ca.ubc.ece.pennyworth.scheduler.experiments;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import ca.ubc.ece.pennyworth.scheduler.Action;
import ca.ubc.ece.pennyworth.scheduler.training.CostModelUtil;
import ca.ubc.ece.pennyworth.scheduler.training.ModelWorkloadGenerator;
import ca.ubc.ece.pennyworth.scheduler.training.QueryGenerator;
import ca.ubc.ece.pennyworth.scheduler.training.decisiontree.DTSearcher;
import ca.ubc.ece.pennyworth.scheduler.training.decisiontree.Trainer;
import ca.ubc.ece.pennyworth.cost.ModelQuery;
import ca.ubc.ece.pennyworth.cost.ModelSLA;
import ca.ubc.ece.pennyworth.cost.QueryTimePredictor;
import ca.ubc.ece.pennyworth.cost.sla.AverageLatencyModelSLA;
import ca.ubc.ece.pennyworth.cost.sla.PerQuerySLA;
import ca.ubc.ece.pennyworth.utilities.StreamUtilities;

public class UnknownLatencyBinExperiment {

	public static void main(String[] args) throws Exception {
		List<Integer> costs = new ArrayList<Integer>();
		List<Integer> spread = new ArrayList<Integer>();
		
		File f = new File("/Users/ryan/spread.csv");
		if (f.exists())
			f.delete();
		
		ModelSLA sla = PerQuerySLA.getLatencyTimesN(2.0);
		Trainer t = new Trainer("/Users/ryan/spread.csv", sla, new QueryGenerator() {
			@Override
			public Set<ModelQuery> generateQueries(int queries) {
				return ModelWorkloadGenerator.randomQueries(120000, 300000, 5, 500, queries);
			}
		});
		t.train(1000, 8);
		t.close();
		
		for (int i = 0; i < 30000; i += 500) {
			spread.add(i);
			
			System.out.println(i);
			int[] val = new int[1];
			for (int k = 0; k < val.length; k++)
				val[k] = getCostForSpread(i);
			
			Arrays.sort(val);
			
			// take the median. in reality, we would use
			// enough training samples to always get the right tree,
			// but for this experiment to go quickly we will sample from
			// the tree space
			costs.add(val[0]);
		}
		
		StreamUtilities.zip(costs.stream(), spread.stream()).forEach(p -> {
			System.out.println(p.getB() + "\t" + p.getA());
		});
	}
	
	private static int getCostForSpread(int spread) throws Exception {
		int min = 120000;
		int max = 300000;
		int types = 5;
		
		Set<ModelQuery> queries = ModelWorkloadGenerator.randomQueries(min, max, types, spread, 5000, 42);
		
		
		
//		printHistogram(queries.stream()
//				.map(q -> ((SetLatencyModelQuery)q).getLatency())
//				.collect(Collectors.toList()),
//				0, 120, 500);
		
		queries = ModelWorkloadGenerator.randomQueries(min, max, types, spread, 15);
		
		return getTreeCost(queries, new QueryGenerator() {

			@Override
			public Set<ModelQuery> generateQueries(int queries) {
				return ModelWorkloadGenerator.randomQueries(min, max, types, spread, queries);
			}
			
		});
	}
	
	private static int getTreeCost(Set<ModelQuery> queries, QueryGenerator qg) throws Exception {
		ModelSLA sla = new AverageLatencyModelSLA(300000, 1);
		

		
		QueryTimePredictor qtp = new QueryTimePredictor();
		DTSearcher dt = new DTSearcher("/Users/ryan/spread.csv", qtp, sla);
		//FirstFitDecreasingGraphSearch ffd = new FirstFitDecreasingGraphSearch(sla, qtp);
		//AStarGraphSearch astar = new AStarGraphSearch(new UnassignedQueryTimeHeuristic(qtp), sla, qtp);
		
		List<Action> dtActions = dt.schedule(queries);
		
//		for (ModelQuery q : queries) {
//			System.out.println( ((SetLatencyModelQuery) q).getLatency() / 1000 );
//		}
		
//		System.out.println(CostModelUtil.getFinalState(queries, dtActions, sla));
//		for (ModelVM vm : CostModelUtil.getFinalState(queries, dtActions, sla).getVMs()) {
//			System.out.println(vm.getQueryLatencies(qtp));
//			
//		}
		
		
		
		return CostModelUtil.getCostForPlan(dtActions, sla);
		
	}
	
//	private static void printHistogram(Collection<Integer> data, int min, int bins, int binSize) {
//		for (int b = 0; b < bins; b++) {
//			final int bin = b;
//			long count = data.stream()
//					.filter(i -> i <= bin * (binSize + 1))
//					.filter(i -> i > bin * binSize)
//					.count();
//			System.out.println((b * binSize) + "\t" + count);
//		}
//	}

}
