// { begin copyright } 
// Copyright Sathish Gopalakrishnan 2019
// 
// This file is part of Pennyworth.
// 
// Pennyworth is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Pennyworth is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Pennyworth.  If not, see <http://www.gnu.org/licenses/>.
// 
// { end copyright } 
 
 

package ca.ubc.ece.pennyworth.scheduler.experiments;

import java.io.File;

import ca.ubc.ece.pennyworth.scheduler.training.decisiontree.Trainer;
import ca.ubc.ece.pennyworth.cost.ModelSLA;
import ca.ubc.ece.pennyworth.cost.TightenableSLA;
import ca.ubc.ece.pennyworth.cost.sla.PercentSLA;

public class TrainingTimeComparision {

	public static void main(String[] args) throws Exception {
		
		System.out.println("Looseness, Average, Total, Per Query");
		
//		for (double d = 1.0; d < 8.0; d += 0.02) {
//			ModelSLA sla1 = new AverageLatencyModelSLA((int) (d * 210000), 1);
//			ModelSLA sla2 = new SimpleLatencyModelSLA((int) (d  * 410000), 1);
//			ModelSLA sla3 = PerQuerySLA.getLatencyTimesN(d);
//			
//			System.out.print(d);
//			System.out.print(",");
//			timeAndPrint(sla1);
//			System.out.print(",");
//			timeAndPrint(sla2);
//			System.out.print(",");
//			timeAndPrint(sla3);
//			System.out.println();
//
//
//		}
		
		for (int i = 540; i > 420; i -= 2) {
			TightenableSLA sla = PercentSLA.nintyTenSLA();
			//TightenableSLA sla = new SimpleLatencyModelSLA(9 * 60 * 1000, 1);
			//TightenableSLA sla = PerQuerySLA.getLatencyTimesN(2.0);
			//TightenableSLA sla = new AverageLatencyModelSLA(9 * 60 * 1000, 1);
			timeAndPrint(sla);
		}
		
	}
	
	private static void timeAndPrint(ModelSLA sla) throws Exception {
		File f = new File("time_compare.csv");
		if (f.exists())
			f.delete();
		
		Trainer t = new Trainer("time_compare.csv", sla);
		long start = System.currentTimeMillis();
		t.train(10000, 6);
		System.out.println((System.currentTimeMillis() - start));
		System.out.println((System.currentTimeMillis() - start));
		System.out.println((System.currentTimeMillis() - start));
		System.out.println((System.currentTimeMillis() - start));

		t.close();
	}

}
