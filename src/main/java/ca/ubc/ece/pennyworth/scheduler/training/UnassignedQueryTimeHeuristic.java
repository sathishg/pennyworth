// { begin copyright } 
// Copyright Sathish Gopalakrishnan 2019
// 
// This file is part of Pennyworth.
// 
// Pennyworth is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Pennyworth is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Pennyworth.  If not, see <http://www.gnu.org/licenses/>.
// 
// { end copyright } 
 
 

package ca.ubc.ece.pennyworth.scheduler.training;

import java.util.Arrays;

import ca.ubc.ece.pennyworth.scheduler.Heuristic;
import ca.ubc.ece.pennyworth.scheduler.State;
import ca.ubc.ece.pennyworth.cost.ModelVM;
import ca.ubc.ece.pennyworth.cost.QueryTimePredictor;

public class UnassignedQueryTimeHeuristic implements Heuristic {

	private QueryTimePredictor qtp;
	private ModelVM[] vms;
	
	public UnassignedQueryTimeHeuristic(QueryTimePredictor qtp) {
		this.qtp = qtp;
		vms = qtp.getNewVMs().toArray(new ModelVM[] {});
	}
	
	
	@Override
	public int predictCostToEnd(State s) {
		int toR = s.getUnassignedQueries().stream().mapToInt(q ->
			 Arrays.stream(vms)
					.mapToInt(vm -> vm.getCostForQuery(qtp, q))
					.min()
					.getAsInt()
		).sum(); 

		//System.out.println("Minimum cost for " + s + " is: " + toR);
		
		return toR;
		
	}

}
