// { begin copyright } 
// Copyright Sathish Gopalakrishnan 2019
// 
// This file is part of Pennyworth.
// 
// Pennyworth is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Pennyworth is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Pennyworth.  If not, see <http://www.gnu.org/licenses/>.
// 
// { end copyright } 
package ca.ubc.ece.pennyworth;

import ca.ubc.ece.pennyworth.scheduler.training.decisiontree.DTSearcher;

/**
 * A cached Pennyworth model (black box)
 *
 */
public class PennyworthCachedModel {
	private DTSearcher dt;
	private String trainingData;
	private WorkloadSpecification wf;
		
	PennyworthCachedModel(DTSearcher dt, String trainingData, WorkloadSpecification wf) {
		this.dt = dt;
		this.trainingData = trainingData;
		this.wf = wf;
	}
	
	DTSearcher getDT() {
		return dt;
	}
	
	public String getDecisionTree() {
		return dt.getTree();
	}
	
	public String getTrainingData() {
		return trainingData;
	}
	
	public WorkloadSpecification getWorkloadSpecification() {
		return wf;
	}
}
