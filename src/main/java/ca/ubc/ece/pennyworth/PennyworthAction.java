// { begin copyright } 
// Copyright Sathish Gopalakrishnan 2019
// 
// This file is part of Pennyworth.
// 
// Pennyworth is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Pennyworth is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Pennyworth.  If not, see <http://www.gnu.org/licenses/>.
// 
// { end copyright } 
package ca.ubc.ece.pennyworth;

/**
 * This is a base class representing actions that Pennyworth may suggest. The
 * two relevant subclasses are AdvisorActionProvision and AdvisorActionAssign. The
 * AdvisorActionProvision action represents provisioning a new VM, and the
 * AdvisorActionAssign action represents assigning a query of a particular type to
 * the most recently provisioned VM.
 * 
 *
 */
public class PennyworthAction {

	
	
}
