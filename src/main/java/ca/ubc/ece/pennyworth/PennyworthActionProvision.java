// { begin copyright } 
// Copyright Sathish Gopalakrishnan 2019
// 
// This file is part of Pennyworth.
// 
// Pennyworth is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Pennyworth is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Pennyworth.  If not, see <http://www.gnu.org/licenses/>.
// 
// { end copyright } 
package ca.ubc.ece.pennyworth;

import ca.ubc.ece.pennyworth.aws.VMType;

/**
 * This action represents provisioning a new VM of a certain type.
 * 
 *
 */
public class PennyworthActionProvision extends PennyworthAction {

	private VMType toProv;
	
	PennyworthActionProvision(VMType toProv) {
		this.toProv = toProv;
	}
	
	/**
	 * Returns the type of VM to provision
	 * @return the VM type
	 */
	public VMType getVMTypeToProvision() {
		return toProv; 
	}
	
	@Override
	public String toString() {
		return "[START " + toProv.toString() + "]";
	}
	
}
