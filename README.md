# Pennyworth

**Pennyworth is a tool for scheduling tasks with latency constraints (deadlines) on cloud computing resources.** The project is named after Batman's butler.

## Features

Pennyworth can be used for cloud computing applications that consist of a set of tasks, and the applications have latency requirements.

In particular, Pennyworth can answer questions related to:

1. How many virtual machines (VMs) are needed for an application?
2. Which tasks should be executed on which VMs?
3. In what order should the tasks be executed?

Pennyworth provides an integrated answer to these problems, and it does so in an *SLA-aware* way. Pennyworth utilizes *decision trees* in order to generate policies that are tailored to an application's workload and service-level objective.